﻿/*
 * Copyright (C) 2014 Cory Todd <corneliustodd@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.IO;
using System.Text;

namespace ByteArrayGen
{
    class Program
    {
        static void Main(string[] args)
        {
            string usage = "ByteArrayGen :: Cory Todd 2014 \n\n" +
                "Convert an ascii string to a copy/pasta byte array.\n"+
                "Usage: \n" +
                "bytearraygen.exe [string_input]";

            if (args.Length == 0)
            {
                Console.WriteLine("String input required");
                Console.WriteLine(usage);
                Console.ReadLine();
                return;
            }

            if ( args.Length > 0 && string.IsNullOrEmpty(args[0]))
            {
                Console.WriteLine("String input required");
                Console.WriteLine(usage);
                Console.ReadLine();
                return;
            }
            
            var result = GetBytes(args[0]);

            StringBuilder s = new StringBuilder();
            s.Append("{");
            for (int i = 0; i < result.Length; i++)
            {
                s.Append(string.Format("0x{0:x}", result[i]));
                if (i != result.Length - 1)
                    s.Append(",");
            }
            s.Append("}");

            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter("byte_array.txt");
                sw.WriteLine(s.ToString());
                System.Diagnostics.Process.Start("byte_array.txt");
                Console.WriteLine("Exported " + s.ToString() + " as byte_array.txt");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if(sw != null)
                    sw.Close();
            }

            Console.ReadLine();

        }

        /// <summary>
        /// Returns a byte array from the provided string.
        /// </summary>
        /// <param name="value"></param>
        /// <returns>byte[]</returns>
        private static byte[] GetBytes(string value)
        {
            return Encoding.ASCII.GetBytes(value);
        }
    }
}
