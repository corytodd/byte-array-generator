ByteArrayGen
============

Generate C style byte arrays from ASCII strings

### Example ###   
string2array.exe "http://google.com"    

### Instructions ###    

Be sure to encapsulate in double quotes (") if your string contains special characters